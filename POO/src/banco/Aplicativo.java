package banco;

public class Aplicativo {

	public static void main(String[] args) {
		
		// Criando instância de Carteira
		CarteiraCliente carteira = new CarteiraCliente();
		
		// Abrir conta - Pessoa Fisica
		PessoaFisica pf = new PessoaFisica("Antonio Carlos", "012345");
	
		
		// Adiciona Carteira - Pessoa fisica
		carteira.insere(pf);		
		
		//imprime carteira
		carteira.getPessoaFisica("012345");
		
		//teste
		System.out.println(carteira.getPessoaFisica("012345").getNome());

	}

	
	
}
