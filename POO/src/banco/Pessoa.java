package banco;

import java.util.Date;

public class Pessoa {
	private String nome;
	private Date dtanasc;
	private String endereco;
	private String senha;
	
	public Pessoa(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public Date getDtanasc() {
		return dtanasc;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
}