package banco;

import java.util.ArrayList;
import java.util.List;

import banco.PessoaFisica;
import banco.PessoaJuridica;

public class CarteiraCliente {	
	
	private List<PessoaFisica> carteirapf;
	private List<PessoaJuridica> carteirapj;
	
	public CarteiraCliente() {
		carteirapf = new ArrayList<PessoaFisica>();
		carteirapj = new ArrayList<PessoaJuridica>();
	}
	
	public void insere (PessoaFisica pf) {
		carteirapf.add(pf);
	}
	
	public void insere(PessoaJuridica pj) {
		carteirapj.add(pj);
	}
	
	public PessoaFisica getPessoaFisica(String cpf) {
		for (PessoaFisica pessoaFisica : carteirapf) {
			if(pessoaFisica.getCpf().equals(cpf)) {
				return pessoaFisica;
			}
		}
		
		return null;
	}
	
	public PessoaJuridica getPessoaJuridica(String cnpj) {
		
		for (PessoaJuridica pessoaJuridica : carteirapj) {
			if(pessoaJuridica.getCnpj().equals(cnpj)) {
				return pessoaJuridica;
			}
		}
		
		return null;
	}
}
